#include "pico/stdlib.h"

/**
 * @brief EXAMPLE - BLINK_C
 *        Simple example to initialise the built-in LED on
 *        the Raspberry Pi Pico and then flash it forever. 
 * 
 * @return int  Application return code (zero for success).
 */
void toggle_led(int led_pin, int sleep_time); // toggle_led declaration

int main() {

    // Specify the PIN number and sleep delay
    const uint LED_PIN   =  25;
    const uint LED_DELAY = 500;

    // Setup the LED pin as an output.
    gpio_init(LED_PIN);
    gpio_set_dir(LED_PIN, GPIO_OUT);

    toggle_led(LED_PIN, LED_DELAY); // Enter toggle function...

    // Should never get here due to infinite while-loop.
    return 0;

}

void toggle_led(int led, int sleep_time) { 

    // Do forever...
    while (true) {
        int read_output = gpio_get(led); // check the state of the LED

        if (read_output == 0) gpio_put(led, 1); // if off, switch on
        else gpio_put(led, 0); // else, switch on.

        sleep_ms(sleep_time); // Make Pico sleep for sleep_time ms long
    }

    return;
}
