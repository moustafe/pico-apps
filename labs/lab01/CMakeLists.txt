cmake_minimum_required(VERSION 3.13)

include(../../pico_sdk_import.cmake)

project(lab01 C CXX ASM)

#set(CMAKE_C_STANDARD 11)
#set(CMAKE_CXX_STANDARD 17)

pico_sdk_init()

add_executable(lab01 
	lab01.c
)


target_link_libraries(lab01 pico_stdlib)

pico_add_extra_outputs(lab01)
#pico_enable_stdio_usb(${PROJECT_NAME} 1)
#pico_enable_stdio_uart(${PROJECT_NAME} 0)
