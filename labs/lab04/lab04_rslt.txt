Thread 1 hit Temporary breakpoint 1, main () at C:\Users\emran\OneDrive\Desktop\COLLEGE\Year Three\microp_attempt2\pico-apps\labs\lab04\lab04.c:63
63	int main() {
info registers
r0             0x200006a0          536872608
r1             0x1000034b          268436299
r2             0x20000238          536871480
r3             0x200002ec          536871660
r4             0x10000264          268436068
r5             0x20041f01          537140993
r6             0x18000000          402653184
r7             0x0                 0
r8             0xffffffff          -1
r9             0xffffffff          -1
r10            0xffffffff          -1
r11            0xffffffff          -1
r12            0x34000040          872415296
sp             0x20042000          0x20042000
lr             0x10000223          268436003
pc             0x1000034a          0x1000034a <main>
xPSR           0x61000000          1627389952
msp            0x20042000          0x20042000
psp            0xfffffffc          0xfffffffc
primask        0x0                 0
basepri        0x0                 0
faultmask      0x0                 0
control        0x0                 0
{"output":"","token":27,"outOfBandRecord":[],"resultRecords":{"resultClass":"done","results":[]}}


disassemble main_asm, +26
Dump of assembler code from 0x10000360 to 0x1000037a:
   0x10000360 <main_asm+0>:	movs	r0, #25
   0x10000362 <main_asm+2>:	bl	0x10000304 <asm_gpio_init>
   0x10000366 <main_asm+6>:	movs	r0, #25
   0x10000368 <main_asm+8>:	movs	r1, #1
   0x1000036a <main_asm+10>:	bl	0x1000030c <asm_gpio_set_dir>
   0x1000036e <loop+0>:	ldr	r0, [pc, #36]	; (0x10000394 <led_set_state+8>)
   0x10000370 <loop+2>:	bl	0x10001384 <sleep_ms>
   0x10000374 <loop+6>:	bl	0x1000037a <sub_toggle>
   0x10000378 <loop+10>:	b.n	0x1000036e <loop>
End of assembler dump.
{"output":"","token":30,"outOfBandRecord":[],"resultRecords":{"resultClass":"done","results":[]}}

-- What is the entry point address (in hex) of the blink_asm application?
The entry point for the blink_asm application is on line 15, or 0xF in hex. This relates to ( 0x81b82d ) in the coretx registers.


--What is the entry point address (in hex) of the “main_asm” function?
The entry point for the main_asm function can be found on line 4, or 0x4 in hex. This relates to ( 0x895942 ) in the cortex registers.

-Describe each of the differences between the assembly that was written for the main_asm function and the version that was executed according to GDB.
