// #define WOKWI // uncomment it using WOKWI
#include <stdio.h>
#include <stdlib.h>
#include "pico/float.h"
#include "pico/double.h"

// /**
//  * @brief LAB_02
//  *        This program calculates PI to eleven decimal 
//  *        places and prints it to console. 
//  *        In this program you can see some of the 
//  *        limitations of using both the double and 
//  *        float datatype.
//  * 
//  * @return void  Application will loop indefinately
//  */

double calc_pi_double( int iter );
float calc_pi_float ( int iter ); 

//////////////////// WOKWI ////////////////////////

// // Setup Function
// void setup() {
//   // Set up Serial port and define WOKWI
//   // Serial1.begin(115200);
//   #ifndef WOKWI
//     // Initialise the IO as we will be using the UART
//     // Only required for hardware and not needed for Wokwi
//     stdio_init_all();
//   #endif
// }

// void loop() {
//   //delay(1000); // this speeds up the simulation in WOKWI
//   // Calculate PI using the double datatype
//   double pi_double = calc_pi_double(100000);
//   printf("PI double : %0.11f\n", pi_double);
//   double error_double = abs(3.14159265359 - pi_double);
//   printf("PI double error : %0.11f\n", error_double);
//   printf("---------------------------------\n");
  
//   // Calculate PI using the float datatype
//   float pi_float = calc_pi_float(100000);
//   printf("PI float : %0.11f\n", pi_float);
//   float error_float = abs(3.14159265359 - pi_float);
//   printf("PI float error : %0.11f\n", error_float);
//   printf("---------------------------------\n");
//   }

//////////////////// WOKWI ////////////////////////


int main() {

    #ifndef WOKWI
        // Initialise the IO as we will be using the UART
        // Only required for hardware and not needed for Wokwi
        stdio_init_all();
    #endif

  // Calculate PI using the double datatype
  double pi_double = calc_pi_double(100000);
  printf("PI double : %0.11f\n", pi_double);
  double error_double = abs(3.14159265359 - pi_double);
  printf("PI double error : %0.11f\n", error_double);
  printf("---------------------------------\n");
  
  // Calculate PI using the float datatype
  float pi_float = calc_pi_float(100000);
  printf("PI float : %0.11f\n", pi_float);
  float error_float = abs(3.14159265359 - pi_float);
  printf("PI float error : %0.11f\n", error_float);
  printf("---------------------------------\n");

    // Returning zero indicates everything went okay.
    return 0;
}


double calc_pi_double( int iter ) { 
  double val = 1.00;
  double base = 1.00;

  // Perform Wallis Product using double datatype
  for ( int i = 0; i < iter; i++ ) { 
    double obj1 = (base + 1) / base;
    double obj2 = (base + 1) / (base + 2); 
    val *= obj1;
    val *= obj2;
    base += 2;
  }

  // Return PI
  return val * 2.0;
}

float calc_pi_float( int iter ) { 
  float val = 1.00;
  float base = 1.00;

  // Perform Wallis Product using float datatype
  for ( int i = 0; i < iter; i++ ) { 
    
    float obj1 = (base + 1) / base;
    float obj2 = (base + 1) / (base + 2); 
    val *= obj1;
    val *= obj2;
    base += 2;
  }

  // Return PI
  return val * 2.0;
}

// End of Program
